const controller = {}

const Post = require('./model/Post');
const sequelize =  require('./model/database');

sequelize.sync()

controller.index = (req, res) => {
    const data = {
        name: "Naho Carmona",
        age: 26,
        city: "Buenos Aires"
    }

    res.json(data);
}

controller.list = async (req,res) => {

    try {
        const response = await Post.findAll()
        .then(function(data){
            const res = { succes: true, message: "Load succes full", data: data}
            return res;
        })
        .catch(error => {
            const res = { succes: false, error: error }
            return res;
        })
        return res.json(response);

    } catch (e) {
       console.log("Error controller.list");
       console.log(e); 
    }
}

controller.create = async (req,res) => {
  
    try {

        const {title, contents, image, category } = req.body
        const response = await Post.create({
            title:title,
            contents:contents,
            image:image,
            category:category
        })
        .then(function(data){
            const res = {
                succes: true, 
                message: "Create succesfull", 
                data: data
            }
            return res;
        })
        .catch(error =>{
            const res = { succes: false, error: error }
            return res;
        })
        res.json(response);
    } catch (e) {
        console.log(e)
    }
}

controller.update = async (req,res) => {
    const { id } = req.params;
    // parameter post
    const { title, contents, image, category } = req.body;
    // update data
    const data = await Post.update({
        title:title,
        contents:contents,
        image:image,
        category:category
    },{
      where: { id: id}
    })
    .then( function (data){
      return data;
    })
    .catch(error => {
      return error;
    })
  
    res.json({ success:true, data: data, message: "Updated successful"});
  
  }
//     try {
//         const idPost = 3;

//         const {title, contents, image, category } = req.body

//         const response = await Post.update({
//             title:title,
//             contents:contents,
//             image:image,
//             category:category
//         },{
//             where: {
//                 id: idPost
//             }
//         })
//         .then(function(data){
//             const res = {
//                 succes: true, 
//                 message: "Update succesfull", 
//                 data: data
//             }
//             return res; 
//         })
//         .catch(error => {
//             const res = { succes: false, error: error }
//             return res;
//         });
//         res.json(response);

//     } catch (e) {
//         console.log(e,500);
//     }
// }

controller.get = async (req,res) => {
    try {
        const { id } = req.params;
        const response = await Post.findAll({
            where: { id: id }
        })
        .then(function(data){
            const res = { succes: true, data: data }
            return res;
        })
        .catch(error => {
            const res = { succes: false, error: error}
            return res;
        })
        res.json(response);

    } catch (e) {
        console.log(e)
    }
}

controller.delete = async (req,res) => {
    try {
       const { id } = req.params;
       const response = await Post.destroy({
           where: { id: id }
       })
       .then(function(data){
           const res = { succes: true, data: data, message: "Deleted succesful"}
           return res;
       })
       .catch(error => {
           const res = { succes: false, error: error }
           return res;
       });
       res.json(response);

    } catch (e) {
        console.log(e);
    }
}

module.exports = controller;

