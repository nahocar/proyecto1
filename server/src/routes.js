const express = require('express');
const route = express();

const controller = require('./controller');

route.get('/index',controller.index );
route.get('/list',controller.list );
route.post('/create',controller.create);
route.patch('/update',controller.update);
route.get('/get/:id',controller.get);
route.get('/delete/:id',controller.delete);

module.exports = route;