const Sequelize = require('sequelize');
const sequelize = require('./database');

// const nametable = 'post';

const Post = sequelize.define('post',{
    
    id:{
        type: Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    title: Sequelize.STRING,
    contents: Sequelize.TEXT,
    image: Sequelize.STRING,
    category: Sequelize.STRING,
});

module.exports = Post;