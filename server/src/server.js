//importing express
const express = require('express');
const app = express();

//setting port
app.set('port',process.env.PORT || 3000);

//Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/',(req,res) => {
    res.send("Hello word")
});

app.listen(app.get('port'),() => {
    console.log("Staring server on port "+app.get('port'));
});

const routes = require('./routes');
app.use('/test',routes);







